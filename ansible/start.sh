#!/bin/bash -e

# env to handle the case when we don't want to build a new image
# and just want to deploy an existing tag
export IS_REDEPLOY=false

# infra provisioning and setup
sudo ansible-playbook infra.yml
sudo ansible-playbook -i hosts setup_machines.yml

# build docker images
if [ "$IS_REDEPLOY" == false ] ; then
  echo "building project"
  sudo ansible-playbook update_tags.yml
  sudo ansible-playbook -i hosts build.yml
fi

# deploy new service
sudo ansible-playbook -i hosts deploy.yml
